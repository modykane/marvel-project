console.log('Hello! Edit me in js/personnages.js');

const BASE_URL = 'https://gateway.marvel.com/v1/public/characters';
const PUBLIC_KEY = '1ffc025bb048268434eac30d62536d7b';
const HASH = '65f06383e36e95e0b9ee29e023f5a526';
const TIME_STAMP = 1234567890;
const LIMIT = 6;
const OFFSET = 100;
const url = BASE_URL + '?ts=' + TIME_STAMP + '&apikey=' + PUBLIC_KEY + '&hash=' + HASH + '&limit=' + LIMIT + '&offset=' + OFFSET;
const page = parseInt(new URLSearchParams(window.location.search).get('page') || 1);
//console.log(page);
//const href = new URL(window.location.href);

var content = document.getElementById('js-filter-content');

fetchMarvelApiData(url);

async function fetchMarvelApiData(url) {
    const response = await fetch(url);
    if (response.ok) {
        const json = await response.json();
    } else {
        console.error(response);
    }
}

const favoritePersonnagesJson = localStorage.getItem('favorites');
const favoritePersonnages = favoritePersonnagesJson ? new Map(Object.entries(JSON.parse(favoritePersonnagesJson))) : new Map();
console.log(favoritePersonnages);
const favoriteElements = document.getElementsByClassName('js-favorite');

for (let index = 0; index < favoriteElements.length; index++) {
    const element = favoriteElements.item(index);
    element.addEventListener('click', function () {
        element.classList.toggle("press");
        const parentElement = element.parentElement;

        const personnage = {
            id: parentElement.dataset.id,
            name: parentElement.dataset.name,
            description: parentElement.dataset.description,
            image: parentElement.dataset.image,
            comics: parentElement.dataset.comics,
            series: parentElement.dataset.series,
            stories: parentElement.dataset.stories,
            events: parentElement.dataset.events
        };

        favoritePersonnages.set(personnage.id, personnage);
        localStorage.setItem('favorites', JSON.stringify(Object.fromEntries(favoritePersonnages)));

    });
}
