console.log('Hello! Edit me in js/favorites.js');

const BASE_URL = 'https://gateway.marvel.com/v1/public/characters';
const PUBLIC_KEY = '1ffc025bb048268434eac30d62536d7b';
const HASH = '65f06383e36e95e0b9ee29e023f5a526';
const TIME_STAMP = 1234567890;
const LIMIT = 6;
const OFFSET = 100;
const url = BASE_URL + '?ts=' + TIME_STAMP + '&apikey=' + PUBLIC_KEY + '&hash=' + HASH + '&limit=' + LIMIT + '&offset=' + OFFSET;
const page = parseInt(new URLSearchParams(window.location.search).get('page') || 1);
const href = new URL(window.location.href);

const favoritesContent = document.getElementById('js-favorites-content');
const favoritePersonnagesJson = localStorage.getItem('favorites');

if (favoritePersonnagesJson != null) {
    const favoritePersonnagesObject = JSON.parse(favoritePersonnagesJson);
    const favoritePersonnagesMap = new Map(Object.entries(favoritePersonnagesObject));
    let output = "";
    favoritePersonnagesMap.forEach(personnage => {
        output +=
            '<div class="col">' +
            '<div class="card h-100">' +
            '<img class="img-picture card-img-top"' + ' alt="Picture of ' + personnage.name +
            '" src="' + personnage.image +
            '" href="' + href.origin + "/personnage/" + personnage.id + '">' +
            '<div class="card-body">' +
            '<a class="btn btn-link custom-effect"' +
            '" href="' + href.origin + "/personnage/" + personnage.id + '">' +
            '<h5 class="card-title">' + personnage.name + '</h5>' +
            '</a>' +
            '<p class="card-text">';
        if (personnage.description !== "") {
            output += personnage.description;
        } else {
            output += "Aucune description 😔 🤷‍♂️";
        }
        output += '</p>' +
            '</div>' +
            '<div class="card-footer">' +
            '<small class="text-muted">Data provided by Marvel. © 2022 MARVEL</small>' +
            '</div>' +
            '</div>' +
            '</div>';
    });

    favoritesContent.innerHTML = output;
} else {
    favoritesContent.innerHTML = '<div class="container"><div class="p-4">' +
        '<h2 class="justify-content text-white">Aucun personnages favoris 😔 </h2>' +
        '<p class="justify-content text-white">Veuillez d\'abord sélectionner vos personnages au niveau de la page "Personnages".</p>' +
        '</div></div>';
}

