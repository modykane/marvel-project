<?php

namespace App\Tests\Functional\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testShouldVisitLoggedInPage(): void
    {
        $client = static::createClient();
    
        // Simule la visite de la page de connexion
        $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Connexion');
    }

    /* public function testVisitingWhileLoggedOutPage(): void
    {
        $client = static::createClient();
        
        // Récupère le le service UserRepository
        $userRepository = static::getContainer()->get(UserRepository::class);

        // Récupère l'utilisateur de test avec son email
        $testUser = $userRepository->findOneByEmail('user@test.com');

        // Simule l'utilisateur de test étant connecté
        $client->loginUser($testUser);
    
        // Simule la visite de la page d'un personnage
        $client->request('GET', '/logout');

        $this->assertResponseIsSuccessful();
        //$this->assertSelectorTextContains('h1', 'Détails du personnage');
    } */
}
