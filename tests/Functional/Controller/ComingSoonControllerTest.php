<?php

namespace App\Tests\Functional\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ComingSoonControllerTest extends WebTestCase
{
    public function testShouldVisitIndexPageWhileLoggedIn(): void
    {
        $client = static::createClient();

        // Récupère le le service UserRepository
        $userRepository = static::getContainer()->get(UserRepository::class);

        // Récupère l'utilisateur de test avec son email
        $testUser = $userRepository->findOneByEmail('user@test.com');

        // Simule l'utilisateur de test étant connecté
        $client->loginUser($testUser);
    
        // Simule la visite de la page /coming-soon
        $client->request('GET', '/comingsoon');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Coming soon...');
    }

    public function testShouldVisitShowPageWhileLoggedIn(): void
    {
        $client = static::createClient();
        
        // Récupère le le service UserRepository
        $userRepository = static::getContainer()->get(UserRepository::class);

        // Récupère l'utilisateur de test avec son email
        $testUser = $userRepository->findOneByEmail('user@test.com');

        // Simule l'utilisateur de test étant connecté
        $client->loginUser($testUser);
    
        // Simule la visite de la page /coming-soon/1009175
        $client->request('GET', '/comingsoon/1009175');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Coming soon...');
    }
}
