<?php

namespace App\Tests\Functional\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PersonnageControllerTest extends WebTestCase
{
    /* public function testShouldVisitIndexPageWhenNotLoggedIn(): void
    {
        $client = static::createClient();
    
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('a', '/login');
        // $response = $client->getResponse()->getContent();
        // $this->assertStringContainsString("Personnages", $response);
    } */

    public function testShouldVisitIndexPageWhileLoggedIn(): void
    {
        $client = static::createClient();
       
        // Récupère le le service UserRepository
        $userRepository = static::getContainer()->get(UserRepository::class);

        // Récupère l'utilisateur de test avec son email
        $testUser = $userRepository->findOneByEmail('user@test.com');

        // Simule l'utilisateur de test étant connecté
        $client->loginUser($testUser);
    
        // Simule la visite de la page d'accueil
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Personnages');
    }

    public function testShouldVisitShowPageWhileLoggedIn(): void
    {
        $client = static::createClient();
        
        // Récupère le le service UserRepository
        $userRepository = static::getContainer()->get(UserRepository::class);

        // Récupère l'utilisateur de test avec son email
        $testUser = $userRepository->findOneByEmail('user@test.com');

        // Simule l'utilisateur de test étant connecté
        $client->loginUser($testUser);
    
        // Simule la visite de la page d'un personnage
        $client->request('GET', '/personnage/1009175');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Détails du personnage');
    }
}
