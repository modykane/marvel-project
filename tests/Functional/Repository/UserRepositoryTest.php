<?php

namespace App\Tests\Functional\Repository;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserpRepositoryTest extends KernelTestCase
{
    public function testShouldBeCreateNewUser(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        $userRepository = static::getContainer()->get(UserRepository::class);

        $this->assertInstanceOf(UserRepository::class, $userRepository);

        $user1 = new User();
        $user1->setFirstName('firstName1')
            ->setLastName('lastName1')
            ->setEmail('user1@email.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt(new \DateTime());

        $userRepository->add($user1);
    }

    public function testShouldBeCreateNewUserAndRemove(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        $userRepository = static::getContainer()->get(UserRepository::class);

        $this->assertInstanceOf(UserRepository::class, $userRepository);

        $user2 = new User();
        $user2->setFirstName('firstName2')
            ->setLastName('lastName2')
            ->setEmail('user2@email.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt(new \DateTime());

        $userRepository->add($user2);

        $userRepository->remove($user2);
    }

    public function testShouldBeCreateNewUserAndUpgradePasswordWithValidUser(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        $userRepository = static::getContainer()->get(UserRepository::class);

        $this->assertInstanceOf(UserRepository::class, $userRepository);

        $user3 = new User();
        $user3->setFirstName('firstName3')
            ->setLastName('lastName3')
            ->setEmail('user3@email.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt(new \DateTime());

        $userRepository->add($user3);

        $userRepository->upgradePassword($user3, 'newpassword');
    }
}