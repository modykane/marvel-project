<?php

namespace App\Tests\External\Service;

use App\Exception\MarvelAPIException;
use App\Service\MarvelAPIPersonnageService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MarvelAPIPersonnageServiceTest extends KernelTestCase
{
    private MarvelAPIPersonnageService $marvelAPIPersonnageService;
    
    protected function setUp(): void
    {
        self::bootKernel();
        $this->marvelAPIPersonnageService = static::getContainer()->get(MarvelAPIPersonnageService::class);
    }

    /* protected function tearDown(): void
    {
        $this->marvelAPIPersonnageService = null;
    } */

    public function testShouldThrowExceptionForEmptyPesonnageIdArgument()
    {
        $this->expectException(MarvelAPIException::class);
        $this->expectExceptionMessage('ID Personnage is required.');
        $this->expectExceptionCode(400);
 
        $this->marvelAPIPersonnageService->getPersonnage(0);
    }

    public function testShouldThrowExceptionForInvalidPesonnageIdArgument()
    {
        $id = 12345;
 
        $this->expectException(MarvelAPIException::class);
        $this->expectExceptionCode(400);
 
        $this->marvelAPIPersonnageService->getPersonnage($id);
    }

    public function testShouldThrowExceptionForInvalidPesonnagesRequest()
    {
        $limit = 12345;
        $offset = 12345;
 
        $this->expectException(MarvelAPIException::class);
        $this->expectExceptionCode(400);
 
        $this->marvelAPIPersonnageService->getPersonnages($limit, $offset);
    }

    public function testShouldReturnTheDataOfAllPesonnages()
    {
        $result = $this->marvelAPIPersonnageService->getPersonnages();
        $this->assertIsArray($result);
        $this->assertEquals(200, $result['code']);
        $this->assertEquals('Ok', $result['status']);
        $this->assertEquals(20, count($result['data']['results']));
    }

    public function testShouldReturnTheDataForTheGivenPesonnageId()
    {
        $result = $this->marvelAPIPersonnageService->getPersonnage(1009175);
        $this->assertIsArray($result);
        $this->assertEquals(200, $result['code']);
        $this->assertEquals('Ok', $result['status']);
        $this->assertEquals(1, count($result['data']['results']));
    }
}
