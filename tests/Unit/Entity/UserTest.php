<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testShouldBeTrue(): void
    {
        $user = new User();
        $createdAt = new \DateTime();
        $user->setFirstName('firstName')
            ->setLastName('lastName')
            ->setEmail('true@email.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt($createdAt);
        

        $this->assertEquals('firstName', $user->getFirstName());
        $this->assertEquals('lastName', $user->getLastName());
        $this->assertEquals('true@email.com', $user->getEmail());
        $this->assertEquals('password', $user->getPassword());
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
        $this->assertEquals($createdAt, $user->getCreatedAt());
    }

    public function testShouldBeFalse(): void
    {
        $user = new User();
        $user->setFirstName('firstName')
            ->setLastName('lastName')
            ->setEmail('true@email.com')
            ->setPassword('password')
            ->setRoles(['ROLE_USER']);
        

        $this->assertNotEquals('false', $user->getFirstName());
        $this->assertNotEquals('false', $user->getLastName());
        $this->assertNotEquals('false@email.com', $user->getEmail());
        $this->assertNotEquals('false', $user->getPassword());
        $this->assertNotEquals(['false'], $user->getRoles());
        $this->assertNotEquals('false', $user->getCreatedAt());
    }

    public function testShouldBeEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getCreatedAt());
        $this->assertEmpty($user->getId());
    }
}
