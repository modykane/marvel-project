<?php

namespace App\Tests\Mock;

use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

class MarvelAPIMockHttpClient implements HttpClientInterface
{
    private array $responses;
    
    public function __construct(array $responses = [])
    {
        $this->responses = $responses;
    }
  
    /**
     * {@inheritdoc}
     */
    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $response = $this->responses[$url] ?? null;
    
        if (null === $response) {
            throw new \LogicException(sprintf('There is no response for url: %s', $url));
        }

        return (new MockHttpClient($response, 'https://api.exemple.com'))->request($method, $url);
    }
    
    /**
     * {@inheritdoc}
     */
    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        throw new \LogicException(sprintf('%s() is not implemented', __METHOD__));
    }

    /**
     * {@inheritdoc}
     */
    public function withOptions(array $options): static
    {
        return $this;
    }
}
