
# 🦸🏻‍♀️🥷 Peak's Up Marvel 🥷🦸🏻‍♀️

## Description 📖

Création d'un projet Symfony sur les personnages de Marvel. Voulez-vous pouvoir accéder rapidement et facilement à toutes les informations sur les Personnages, Comics, Séries et bien plus encore ? Cette application est conçue pour vous 🙂😇😈.


## Installation 🖥

**Peak's Up Marvel** est une application basée sur Symfony/PHP.
Cette documentation propose une installation simplifiée pour le développement uniquement.

### Pré-requis 🔍

- [PHP 8.1](https://www.php.net/downloads.php)
- [Composer](https://getcomposer.org/)
- [Symfony CLI](https://symfony.com/download)

### Clone et Installation 🖥

```bash
  git clone https://gitlab.com/modykane/marvel-project.git
  cd marvel-project
  composer install
  npm install --force
  npm run build
  php bin/console d:d:c
  php bin/console d:m:e 'DoctrineMigrations\Version20220411124038' --no-interaction
  symfony serve -d ou php -S localhost:8000 -t public
```

### Lancement des tests 🐛

- Créer la base de données de test
```bash
  php bin/console d:d:c --env=test
  php bin/console d:m:e 'DoctrineMigrations\Version20220411124038' --env=test --no-interaction
``` 

- Lancer les Tests
```bash
  php bin/console doctrine:fixtures:load --env=test --no-interaction  
  php ./vendor/bin/phpunit --testdox
``` 

- Lnacer le Coverage
```bash
  php bin/console doctrine:fixtures:load --env=test --no-interaction  
  XDEBUG_MODE=coverage php ./vendor/bin/phpunit --coverage-html var/coverage/test-coverage
```

## Comment tester l'appli rapidement juste avec Docker 🥷

Nous vous proposons une méthode rapide pour tester l'application avec l'utilisation d'un container Docker préconfiguré.

### Pré-requis 🔍
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Création du fichier docker-compose.yml 📝

```bash
version: '3'

services:
  app:
    image: modykane/docker-marvel-projet:latest
    container_name: app-docker-marvel-projet
    ports:
      - "8080:80"
```

### Lancement le container 📦

```bash
docker compose up -d ou docker-compose up -d
```

### Lancez votre navigateur et amusez-vous 🙂

Aller sur : [http://localhost:8080](http://localhost:8080).
##  Capture d'image 📸

![App Screenshot](assets/img/accueil.png)


## Licence ©️®️

Distribué sous la licence : [MIT License](https://choosealicense.com/licenses/mit/).


## Feedback 👌

Si vous avez des commentaires, veuillez nous contacter à : mkane@peaks.fr

