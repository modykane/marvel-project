<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstName('User')
            ->setLastName('Test')
            ->setEmail('user@test.com')
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt(new \DateTime());

        $password = $this->userPasswordHasher->hashPassword($user, 'usertest');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}
