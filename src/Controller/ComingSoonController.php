<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComingSoonController extends AbstractController
{
    #[Route('/comingsoon', name: 'coming_soon')]
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('coming_soon/index.html.twig', [
            'controller_name' => 'ComingSoonController',
        ]);
    }

    #[Route('/comingsoon/{id}', name: 'coming_soon_show', methods: ['GET'])]
    public function show(int $id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('coming_soon/index.html.twig', [
            'controller_name' => 'ComingSoonController',
        ]);
    }
}
