<?php

namespace App\Controller;

use App\Service\MarvelAPIPersonnageService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class PersonnageController extends AbstractController
{
    #[Route('/', name: 'personnage', methods: ['GET'])]
    public function index(
        MarvelAPIPersonnageService $marvelAPIPersonnageService, 
        CacheInterface $cache, 
        PaginatorInterface $paginator, 
        Request $request
    ): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // Amélioration concernant la performance de l'appel à l'API:
        // Mise en cache des données des personnages pour une durée de 24h
        /* $personnages = $cache->get('personnages', function (ItemInterface $item) use ($marvelAPIPersonnageService, $paginator, $request) {
            $item->expiresAfter(86400);
            return $personnages;
        }); */
        $results = $marvelAPIPersonnageService->getPersonnages();
        $personnages = $paginator->paginate(
            $results['data']['results'], // tableau contenant les données des personnages // 
            $request->query->getInt('page', 1), // numéro de la page : par défaut page 1 // 
            6 // nombre d'éléments par page : par défaut 6 //
        );
        
        return $this->render('personnage/index.html.twig', [
            'personnages' => $personnages,
        ]);
    }
    
    #[Route('/personnage/favorites', name: 'personnage_favorites', methods: ['GET'])]
    public function favorites(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('personnage/favorites.html.twig');
    }

    #[Route('/personnage/{id}', name: 'personnage_show', methods: ['GET'])]
    public function show(int $id, MarvelAPIPersonnageService $marvelAPIPersonnageService, CacheInterface $cache): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // Amélioration concernant la performance de l'appel à l'API:
        // Mise en cache des données des détails du personnage pour une durée de 24h
        $personnage = $cache->get('personnage' . $id, function (ItemInterface $item) use ($id, $marvelAPIPersonnageService) {
            $item->expiresAfter(86400);
            return $marvelAPIPersonnageService->getPersonnage($id);
        });

        return $this->render('personnage/show.html.twig', [
            'personnage' => $personnage,
        ]);
    }
}
