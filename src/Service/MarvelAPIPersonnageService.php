<?php

namespace App\Service;

use App\Exception\MarvelAPIException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MarvelAPIPersonnageService
{
    const BASE_URL = 'https://gateway.marvel.com/v1/public/characters';
    const PUBLIC_KEY = '1ffc025bb048268434eac30d62536d7b';
    const HASH = '65f06383e36e95e0b9ee29e023f5a526';
    const TIME_STAMP = 1234567890;
    private HttpClientInterface $httpClient;
    private string $url;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->url = self::BASE_URL . '?ts=' . self::TIME_STAMP . '&apikey=' . self::PUBLIC_KEY  . '&hash=' . self::HASH;
    }

    /**
     * Retourne les données des 20 personnages de l'API Marvel à partir du 100e personnage
     */
    public function getPersonnages($limit = 20, $offset = 100): array
    {
        $response = $this->httpClient->request('GET', $this->url . '&limit=' . $limit . '&offset=' . $offset);

        if (200 !== $response->getStatusCode()) {
            $this->throwException($response->getContent(false));
        }
        
        return $response->toArray();
    }

    /**
     * Retourne les données d'un personnage de l'API Marvel à partir de son identifiant
     */
    public function getPersonnage(int $id): array
    {
        if (!$id) {
            $this->throwException('ID Personnage is required.');
        }

        $response = $this->httpClient->request('GET', $this->url . '&id=' . $id);
        
        if (200 !== $response->getStatusCode()) {
            $this->throwException($response->getContent(false));
        }
        
        return $response->toArray();
    }

    /**
     * Génère une exception à partir d'un message d'erreur
     */
    private function throwException($message, $code = 400)
    {
        throw new MarvelAPIException($message, $code);
    }
}
